import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../views/home.vue'),
    children:[
      {
        path:"/",
        component:() => import("./../components/track_display.vue")
      },
      {
        path:"/influence",
        component:() => import("./../components/influence.vue")
      },
      {
        path:"/statistics",
        component:() => import('./../components/statistics.vue')
      },
      {
        path:"/reward_punish",
        component:() => import("./../components/reward_punish.vue")
      },
      {
        path:"/test",
        component:() => import("./../components/test.vue")
      },
      {

        path:"/tube",
        component:() => import("./../components/tube.vue")
      },
      {
        path:"/Historical_achievements",
        component:() => import("./../components/Historical_achievements.vue")
      },
      {
        path:"/echarts",
        component:() => import("./../components/track_display/echarts.vue")
      },
      {
        path:"/real_time",
        component:() => import("./../components/track_display/real_time.vue")
      },
      {
        path:"/score_pre",
        component:() => import("./../components/score_pre.vue")
      }
  ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
