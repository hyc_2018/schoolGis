import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import BaiduMap from 'vue-baidu-map'

Vue.config.productionTip = false
Vue.use(ViewUI);
Vue.use(BaiduMap, {
  ak: 'g0fUkIhl8oQIo4WS6ASVVgOqYVqgpwhM'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
